import React, { Component, PropTypes } from 'react';

export default class NoMatchPage extends Component {
  render() {
    return (
    <main className="content container">
      <h1 className="text-center">404 - Page not found</h1>
    </main>
    );
  }
}
