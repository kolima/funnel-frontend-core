import React, { Component, PropTypes} from 'react';
import { Alert } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as alertBarActions from '../../modules/alert';

@connect(
    state => ({
    isSuccessAlertVisible: state.alert.isSuccessAlertVisible
  }),
    dispatch => bindActionCreators(alertBarActions, dispatch)
)

export default
class SuccessAlertBar extends Component {
  constructor(props) {
    super(props);
    this._visibilityTimeout = 2000;
  }

  handleAlertDismiss() {
    this.props.hideSuccessAlert();
  }

  render() {
    const { isSuccessAlertVisible, message = 'Changes successfully saved' } = this.props;

    if (!isSuccessAlertVisible) return false;

    return (
      <Alert className="validation-alert"
             bsStyle="success"
             onDismiss={this.handleAlertDismiss.bind(this)}
             dismissAfter={this._visibilityTimeout}>
        <h4 className="alert-title">{message}</h4>
      </Alert>
    )
  }
}
