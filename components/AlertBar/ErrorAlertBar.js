import React, { Component, PropTypes} from 'react';
import { Alert } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as alertBarActions from '../../modules/alert';

@connect(
    state => ({
    isVisible: state.alert.isVisible
  }),
    dispatch => bindActionCreators(alertBarActions, dispatch)
)

export default
class AlertBar extends Component {
  constructor(props) {
    super(props);
    this._visibilityTimeout = 3000;
  }

  handleAlertDismiss() {
    this.props.hideValidationAlert();
  }

  render() {
    const { isVisible } = this.props;

    if (!isVisible) return false;

    return (
      <Alert className="validation-alert"
             bsStyle="danger"
             onDismiss={this.handleAlertDismiss.bind(this)}
             dismissAfter={this._visibilityTimeout}>
        <h4 className="alert-title">Validation Error Occured</h4>
      </Alert>
    )
  }
}
