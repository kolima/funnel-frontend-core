const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanPlugin = require('clean-webpack-plugin');
const strip = require('strip-loader');
const projectRootDirectory = path.resolve();

const universal = {
  __CLIENT__: true,
  __DEVTOOLS__: false,
  __SERVER__: false,
  API_URL: JSON.stringify('https://kolima-funnel-api.herokuapp.com')
};

module.exports = {
  secure: {
    ssl: false,
    privateKey: './config/sslcerts/key.pem',
    certificate: './config/sslcerts/cert.pem'
  },
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'combined',
    // Stream defaults to process.stdout
    // Uncomment to enable logging to a log on the file system
    options: {
      stream: 'access.log'
    }
  },
  files: {
    build: {
      js: 'build/*.js',
      css: 'build/*.css'
    }
  },
  universal: universal,
  webpack: {
    devtool: 'source-map',
    entry: {
      'main': [
        './public/application'
      ]
    },
    output: {
      path: path.join(projectRootDirectory, 'build'),
      filename: '[name].[hash].js',
      chunkFilename: '[name].[chunkhash].js',
      publicPath: '/build/'
    },
    module: {
      loaders: [
        {
          test: /\.js$/,
          loaders: [strip.loader('debug'), 'babel'],
          exclude: /^(?=.*?\bnode_modules\b)((?!funnel-frontend-core).)*$/,
          include: projectRootDirectory
        }, {
          test: /\.scss$/,
          loader: ExtractTextPlugin.extract('style', 'css?sourceMap!autoprefixer?browsers=last 2 version!sass?sourceMap=true&sourceMapContents=true')
        }, {
          test: /\.css$/,
          loader: 'style-loader!css-loader'
        }, {
          test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url?limit=10000&minetype=application/font-woff'
        }, {
          test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url?limit=10000&minetype=application/font-woff'
        }, {
          test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url?limit=10000&minetype=application/octet-stream'
        }, {
          test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'file'
        }, {
          test: /\.gif$/,
          loader: 'url-loader?mimetype=image/png'
        }, {
          test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url?limit=10000&minetype=image/svg+xml'
        }, {
          test: /masonry|imagesloaded|fizzy\-ui\-utils|desandro\-|outlayer|get\-size|doc\-ready|eventie|eventemitter/,
          loader: 'imports?define=>false&this=>window'
      }],
      noParse: /\.min\.js/
    },
    progress: true,
    resolve: {
      modulesDirectories: [
        'public',
        'node_modules'
      ],
      extensions: ['', '.json', '.js']
    },
    plugins: [
      new webpack.DefinePlugin(universal),

      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),

      new CleanPlugin('build', projectRootDirectory),
      new ExtractTextPlugin('[name].[chunkhash].css', {allChunks: true}),

      // ignore dev config
      new webpack.IgnorePlugin(/\.\/dev/, /\/config$/),

      // optimizations
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      })
    ]
  }
};
