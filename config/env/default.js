'use strict';

module.exports = {
  app: {
    title: 'Funnel',
    description: 'Improve your skill',
    keywords: 'person, sill, learn, english',
    googleAnalyticsTrackingID: process.env.GOOGLE_ANALYTICS_TRACKING_ID
  },
  port: process.env.PORT,
  files: {
    server: {
      routes: ['app/routes/**/!(core.route.js)', 'app/routes/**/core.route.js'],
      views: 'app/views'
    },
    build: {
      js: ['build/main.js'],
      css: ['build/main.css']
    }
  }
};
