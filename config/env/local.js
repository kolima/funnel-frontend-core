const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const projectRootDirectory = path.resolve();

const universal = {
  __CLIENT__: true,
  __DEVTOOLS__: true,
  __SERVER__: false,
  API_URL: JSON.stringify('http://localhost:3010')
};

module.exports = {
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'dev',
    // Stream defaults to process.stdout
    // Uncomment to enable logging to a log on the file system
    options: {
      //stream: 'access.log'
    }
  },
  app: {
    title: 'Funnel - Local Environment'
  },
  universal: universal,
  webpack: {
    devtool: 'cheap-module-eval-source-map',
    entry: [
      'webpack-hot-middleware/client',
      './public/application'
    ],
    output: {
      path: path.join(projectRootDirectory, 'build'),
      filename: 'main.js',
      publicPath: '/build/'
    },
    module: {
      loaders: [{
        test: /\.js$/,
        loaders: ['babel'], //'eslint-loader'
        exclude: /^(?=.*?\bnode_modules\b)((?!funnel-frontend-core).)*$/,
        include: projectRootDirectory
      }, {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style', 'css?sourceMap!autoprefixer?browsers=last 2 version!sass?sourceMap=true&sourceMapContents=true')
      }, {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }, {
        test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&minetype=application/font-woff'
      }, {
        test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&minetype=application/font-woff'
      }, {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&minetype=application/octet-stream'
      }, {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file'
      }, {
        test: /\.gif$/,
        loader: 'url-loader?mimetype=image/png'
      }, {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&minetype=image/svg+xml'
      }, {
        test: /masonry|imagesloaded|fizzy\-ui\-utils|desandro\-|outlayer|get\-size|doc\-ready|eventie|eventemitter/,
        loader: 'imports?define=>false&this=>window'
      }],
      noParse: /\.min\.js/
    },
    progress: true,
    resolve: {
      modulesDirectories: [
        'public',
        'node_modules'
      ],
      extensions: ['', '.json', '.js']
    },
    plugins: [
      new webpack.DefinePlugin(universal),
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new ExtractTextPlugin('main.css')
    ]
  }
};
