'use strict';

const isEmpty = value => value === undefined || value === null || value === '';
const join = (rules) => (value, values) => rules.map(rule => rule(value, values)).filter(error => !!error)[0]; //first error

export function email(value) {
  if (!isEmpty(value) && !/^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/i.test(value)) {
    return 'Invalid email address';
  }
}

export function required(value) {
  if (isEmpty(value)) {
    return 'Required';
  }
}

export function selected(value) {
  if (!value) {
    return 'Required';
  }
}

export function minLength(min) {
  return value => {
    if (!isEmpty(value) && value.length < min) {
      return `Must be at least ${min} characters`;
    }
  };
}

export function maxLength(max) {
  return value => {
    if (!isEmpty(value) && value.length > max) {
      return `Must be no more than ${max} characters`;
    }
  };
}

export function integer(value) {
  if (!Number.isInteger(Number(value))) {
    return 'Must be an integer';
  }
}

export function oneOf(enumeration) {
  return value => {
    if (!~enumeration.indexOf(value)) {
      return `Must be one of: ${enumeration.join(', ')}`;
    }
  };
}

export function equal(name) {
  return (value, values) => {
    if (!values || (value !== values[name])) {
      return `Must be equal ${name}`;
    }
  };
}

export function createValidator(rules) {
  return (data = {}) => {
    const errors = {valid: true};
    Object.keys(rules).forEach((key) => {
      const dynamicKey = key.split('.');

      if ((dynamicKey.length === 2) && (dynamicKey[1] === '_dynamic')) {

        Object.keys(data).forEach((el) => {
          let parsedElement = el.split('.');

          if ((parsedElement.length === 2) && (parsedElement[0] === dynamicKey[0])) {
            let rule = join([].concat(rules[key]));
            let error = rule(data[el]);

            if (error) {
              errors[el] = error;
              errors.valid = false;
            }
          }
        });
      } else {
        let rule = join([].concat(rules[key]));
        let error = rule(data[key], data);

        if (error) {
          errors[key] = error;
          errors.valid = false;
        }
      }

    });
    return errors;
  };
}
